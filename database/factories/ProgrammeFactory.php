<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Programme;
use Faker\Generator as Faker;
use App\Models\Channel;

$factory->define(Programme::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'description' => $faker->text(1000),
        'url_thumbnail' => $faker->imageUrl(),
        'start' => $faker->datetime('now', 'UTC'),
        'end' => $faker->dateTimeInInterval('now', '+ 30 minutes', 'UTC'),
        'duration' => $faker->numberBetween(1,3600),
        'channel_id' => Channel::firstOrFail()->channel_id
    ];
});
