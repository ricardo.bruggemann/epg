<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Programmes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('programmes', function (Blueprint $table) {
          $table->id('programme_id');
          $table->uuid('uuid')->unique();
          $table->string('name', 60);
          $table->text('description');
          $table->string('url_thumbnail', 200);
          $table->dateTime('start');
          $table->dateTime('end');
          $table->integer('duration'); // Seconds
          $table->timestamps();
          $table->unsignedBigInteger('channel_id');

          $table->index('channel_id', 'p_channel_id_idx');
          $table->foreign('channel_id', 'p_channel_id_fk')->references('channel_id')->on('channels');

          $table->index(['start', 'end'], 'p_date_start_end_idx');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmes');
    }
}
