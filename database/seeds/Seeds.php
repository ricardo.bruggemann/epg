<?php

use Illuminate\Database\Seeder;

class Seeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\Channel', 5)->create();
        factory('App\Models\Programme', 50)->create();
    }
}
