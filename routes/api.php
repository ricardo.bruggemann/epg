<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/channels', 'ChannelController@list')->name('channels.list');

Route::put('/channels/store', 'ChannelController@store')->name('channels.store');
Route::put('/programme/store', 'ProgrammeController@store')->name('programme.store');

Route::get('/channels/{uuid}/{date}/timezone/{timezone}', 'ChannelController@channelProgramme')->name('channel.programme');
Route::get('/channels/{channeluuid}/programmes/{programmeuuid}', 'ChannelController@programmeDetail')->name('programme.details.index');
