<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use DB;

class ChannelTest extends TestCase
{
    /** @test */
    public function user_can_get_data()
    {
        // Generate the data
        $attributes = factory('App\Models\Channel')->raw();

        // Send
        $response = $this->get(route('channels.list'));

        // Assert
        $response->assertStatus(200)->assertSee("data");
    }

    /** @test */
    public function record_store_has_name()
    {
        // Generate the data
        $attributes = factory('App\Models\Channel')->raw(["name" => ""]);

        // Send
        $response = $this->put(route('channels.store'), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_store_has_max_60_name()
    {
        // Generate the data
        $attributes = factory('App\Models\Channel')->raw(["name" => "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyu"]);

        // Send
        $response = $this->put(route('channels.store'), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

}
