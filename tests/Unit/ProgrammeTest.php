<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Programme;
use DB;

class ProgrammeTest extends TestCase
{
    /** @test */
    public function user_can_store()
    {
        // Generate the data
        $attributes = factory('App\Models\Programme')->raw();

        // Send
        $response = $this->put(route('programme.store'), $attributes);

        // Assert
        $response->assertStatus(201)->assertSee("success");
    }

}
