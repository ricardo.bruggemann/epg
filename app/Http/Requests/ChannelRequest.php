<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class ChannelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['name' => ['required', 'between:1,60' ],
                'url_icon' => ['required', 'between:1,200'],
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException (Response::json(array(
            'title'        => __('fw.val.return.title'),
            'message'      => __('fw.val.return.message'),
            'errors'       => $validator->getMessageBag()->toArray(),
            'type'         => 3,
            'tp_message'   => "error" // success // error // info // warning
      ), 422));
    }
}
