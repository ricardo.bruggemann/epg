<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Programme;
use App\Http\Filters\ProgrammeFilter;
use App\Http\Requests\ProgrammeRequest;


class ProgrammeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Programme $programme, ProgrammeRequest $request)
    {
        $programme->create($request->all());

        return $this->returnSuccess();
    }
}
