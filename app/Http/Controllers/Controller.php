<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * Return message of sucess pattern simplert
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function returnSuccess($statusResponse = 200, $typeComponent = 2, $status = "success")
    {
        if(request()->method()=="PUT")
        {
            $statusResponse = 201;
        }
        elseif(request()->method()=="PATCH")
        {
            $statusResponse = 200;
        }
        elseif(request()->method()=="DELETE")
        {
            $statusResponse = 202;
        }

        return \Response::json(['title'        => __("fw.".request()->method().".return.title"),
                                'message'      => __("fw.".request()->method().".return.message"),
                                'errors'       => "",
                                'type'         => $typeComponent, // 1 Snotify; 2 = SimpleAert; 3 = Nulo
                                'tp_message'   => $status // success // error // info // warning
                            ], $statusResponse);
    }

    /**
    * Return message of sucess snotify to actiavate
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function returnStatusActivate($statusResponse = 200, $typeComponent = 1, $status = "success")
    {
        return \Response::json(['title'        => __('fw.status.return.activate'),
                                'message'      => __('fw.status.return.message', ['status_descr' => __('fw.status.return.activate')]),
                                'type'         => $typeComponent, // Snotify
                                'tp_message'   => $status, // success // error // info // warning
                               ], $statusResponse);
    }

    /**
    * Return message of sucess snotify to desactiavate
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function returnStatusDesactivate($statusResponse = 200, $typeComponent = 1, $status = "warning")
    {
        return \Response::json(['title'        => __('fw.status.return.activate'),
                                'message'      => __('fw.status.return.message', ['status_descr' => __('fw.status.return.desactivate')]),
                                'type'         => $typeComponent, // Snotify
                                'tp_message'   => $status, // success // error // info // warning
                               ], $statusResponse);
    }

    /**
    * Return paginate of data in json
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function returnPaginate($items)
    {
        return response()->json($items);
    }
}
