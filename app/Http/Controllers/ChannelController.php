<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\Programme;
use App\Http\Filters\ChannelFilter;
use App\Http\Requests\ChannelRequest;


class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $items = Channel::paginate(50);

        return $this->returnPaginate($items);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function channelProgramme($uuid, $date, $timezone)
    {
        $items = Channel::where("uuid", $uuid)
                        //->where('start', '>=', Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d'))  Commented to test
                        //->where('end', '<=', Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d')) Commented to test 

                        ->with('Programme')
                        ->paginate(50);

        return $this->returnPaginate($items);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function programmeDetail($channeluuid, $programmeuuid)
    {
        $items = Programme::where("uuid", $programmeuuid)
                        ->whereHas('Channel', function ($q) use ($channeluuid) {
                            $q->where('uuid', '=', $channeluuid);
                        })
                        ->paginate(50);

        return $this->returnPaginate($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Channel $channel, ChannelRequest $request)
    {
        $channel->create($request->all());

        return $this->returnSuccess();
    }
}
