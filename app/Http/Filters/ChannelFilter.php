<?php

namespace App\Http\Filters;

class ChannelFilter extends QueryFilter
{
    /**
     * Filter the query by specific code
     *
     * @param  string $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function uuid($value)
    {
        return $this->builder->where('uuid', $value);
    }
}
