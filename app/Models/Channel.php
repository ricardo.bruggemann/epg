<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuditOperations;
use App\Traits\Filterable;
use App\Traits\AutomaticUuid;

class Channel extends Model
{
    use AuditOperations, Filterable, AutomaticUuid;
    protected $table = 'channels';
    protected $primaryKey = 'channel_id';
    protected $fillable = ['name', 'url_icon'];


    /**
    * HasMany programme
    */
    public function Programme()
    {
        return $this->hasMany('App\Models\Programme', 'channel_id', 'channel_id');
    }
}
