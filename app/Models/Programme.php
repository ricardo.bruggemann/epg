<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuditOperations;
use App\Traits\Filterable;
use App\Traits\AutomaticUuid;

class Programme extends Model
{
    use AuditOperations, Filterable, AutomaticUuid;
    protected $table = 'programmes';
    protected $primaryKey = 'programme_id';
    protected $fillable = ['name', 'description', 'url_thumbnail', 'start', 'end', 'duration', 'channel_id'];

    /**
    * Belongs to Channel
    */
    public function Channel()
    {
        return $this->belongsTo('App\Models\Channel', 'channel_id', 'channel_id');
    }
}
