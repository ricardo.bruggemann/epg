<?php
namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait AutomaticUuid
{
    public static function bootAutomaticUuid()
    {

        static::creating(function ($model) {
            $model->uuid = Uuid::uuid4();
        });
    }
}
