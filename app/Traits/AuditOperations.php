<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

trait AuditOperations
{
    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->updated_at = Carbon::now();
        });

        static::creating(function ($model) {
            $model->created_at = Carbon::now();
        });

    }

}
