<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during execution of system
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'val.return.title' => 'Error',
    'val.return.message' => 'Error validating fields.',
    'val.return.btnCloseText' => 'Close',

    'create.return.title' => 'Record created',
    'create.return.message' => 'Record created successfuly.',
    'create.return.btnCloseText' => 'Close',

    'update.return.title' => 'Record Updated',
    'update.return.message' => 'Record updated successfuly.',
    'update.return.btnCloseText' => 'Close',

    'status.return.activate' => 'done',
    'status.return.desactivate' => 'returned to pending',
    'status.return.action' => 'Action',
    'status.return.message' => 'Records(s) :status_descr successfuly!',

    'delete.return.title' => 'Record deleted',
    'delete.return.message' => 'Record deleted successfuly.',
    'delete.return.btnCloseText' => 'Close',

    'PUT.return.title' => 'Record created',
    'PUT.return.message' => 'Record created successfuly.',
    'PUT.return.btnCloseText' => 'Close',

    'PATCH.return.title' => 'Record Updated',
    'PATCH.return.message' => 'Record updated successfuly.',
    'PATCH.return.btnCloseText' => 'Close',

];
