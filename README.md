# Installation
1) Clone repository prepared with Docker and Application
```
git clone https://gitlab.com/ricardo.bruggemann/epg.git
```

2) Download Laravel libraries
```
composer install
```

3) Config .env
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:U3NfAMSQuY6p/eemdAgG09226J3Oml4kJwuJJBH3ygo=
APP_DEBUG=true
APP_URL=http://epg.test

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=todo_mysql_1
DB_PORT=3306
DB_DATABASE=todo
DB_USERNAME=root
DB_PASSWORD=a

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```
4) Run migrations
```
php artisan migrate
```

5) Run seeds
```
php artisan db:seed
```

5) Run tests
```
php artisan test
```
